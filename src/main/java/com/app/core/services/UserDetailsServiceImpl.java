package com.app.core.services;

import com.app.core.databases.mongo.repositories.UsersRepository;
import com.app.core.models.AppUser;
import com.app.core.utils.context.Messages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        AppUser appUser = usersRepository.findByUsername(username);
        if (appUser == null) {
            throw new UsernameNotFoundException(Messages.getMessage("error.user.not.found", username));
        }

        return new User(appUser.getUsername(), appUser.getPassword(),
                AuthorityUtils.createAuthorityList(appUser.getRole()));
    }
}
