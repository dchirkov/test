package com.app.core.utils.lang;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

@Component
public class JsonMapper extends ObjectMapper {

    private static final long serialVersionUID = -5911340893108554677L;

    private String dateFormatPattern;

    public JsonMapper() {
        // {@code ObjectMapper} default constructor call
        super();

        // prevents <tt>null</tt> and empty values in serialized output
        setSerializationInclusion(JsonInclude.Include.NON_EMPTY);

        // prevents <tt>null</tt> entities in serialized output of {@code Map}
        configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

        // configure the format to prevent writing of the serialized output for {@code java.util.Date}
        // instances as timestamps
        configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public final String getDateFormatPattern() {
        return dateFormatPattern;
    }

    public final void setDateFormatPattern(final String dateFormatPattern) {
        this.dateFormatPattern = dateFormatPattern;
        setDateFormat(new SimpleDateFormat(dateFormatPattern));
    }

}