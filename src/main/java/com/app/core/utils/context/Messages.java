package com.app.core.utils.context;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public final class Messages {

    private Messages() {
    }

    private static MessageSource messageSource;

    public static String getMessage(final String code, final Object... args) {
        return messageSource.getMessage(code, args, code, LocaleContextHolder.getLocale());
    }

    @Autowired
    public final void setMessageSource(MessageSource source) {
        messageSource = source;
    }
}
