package com.app.core.databases.mongo.repositories;

import com.app.core.models.AppUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface UsersRepository extends CrudRepository<AppUser, String> {

    AppUser findByUsername(String username);

}
