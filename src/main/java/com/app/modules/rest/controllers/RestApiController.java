package com.app.modules.rest.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestApiController {

    @RequestMapping(value = "/rest/", method = RequestMethod.GET)
    public final ResponseEntity<String> getUser() {

        return ResponseEntity.ok("Rest");
    }

}
