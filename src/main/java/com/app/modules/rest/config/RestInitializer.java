package com.app.modules.rest.config;

import com.app.core.config.ApplicationConfiguration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import org.springframework.web.util.Log4jConfigListener;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

@Order(1)
public class RestInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected String getServletName() {
        return "rest";
    }

    @Override
    protected void customizeRegistration(ServletRegistration.Dynamic registration) {
        registration.setInitParameter("throwExceptionIfNoHandlerFound", "true");
    }

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfiguration.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{RestConfiguration.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/rest/*", "/oauth/*"};
    }

    @Override
    protected void registerDispatcherServlet(ServletContext servletContext) {
        super.registerDispatcherServlet(servletContext);

        servletContext.addListener(Log4jConfigListener.class);
        servletContext.setInitParameter("log4jConfigLocation", "classpath:log4j.properties");
    }
}
