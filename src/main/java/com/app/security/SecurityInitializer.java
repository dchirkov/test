package com.app.security;

import org.springframework.core.annotation.Order;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

@Order(3)
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
