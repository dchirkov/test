<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title><spring:message code="app.title"/>: Error</title>
    <%@ include file="include/ui-resources.jsp" %>
</head>
<body>
<!-- add header -->
<%@ include file="include/header.jsp" %>
<!-- add header -->

<section class="container">
    <div class="col-md-4">
        <div class="row">
            <h4>Error</h4>
        </div>
        <div class="row">
            <c:choose>
                <c:when test="${message ne null and not empty message}">
                    ${message}
                </c:when>
                <c:otherwise>
                    <spring:message code="error.not.found"/>
                </c:otherwise>
            </c:choose>
        </div>
    </div>
</section>
<!-- add footer -->
<%@ include file="include/footer.jsp" %>
<!-- add footer -->
</body>
</html>