<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title><spring:message code="app.title"/>: Login</title>
    <%@ include file="include/ui-resources.jsp" %>
</head>
<body>

<!-- add header -->
<%@ include file="include/header.jsp" %>
<!-- add header -->

<section class="container login-user">
    <div class="col-md-4 col-md-offset-4" role="form">
        <div class="panel panel-default">
            <div class="panel-heading">Login</div>
            <div class="panel-body">
                <c:url var="loginUrl" value="/login"/>
                <form name='LoginForm' action='${loginUrl}' method='post'>
                    <div class="form-group">
                        <label for="user">User:</label>
                        <input id="user" type='text' class="form-control" name='username' value=''/>
                    </div>
                    <div class="form-group">
                        <label for="pwd">Password:</label>
                        <input id="pwd" type='password' class="form-control" name='password'/>
                    </div>
                    <div class="form-group">
                        <label for="remember">Remember me:</label>
                        <input id="remember" type='checkbox' name='remember'/>
                    </div>
                    <c:if test="${error}">
                        <c:if test="${cause ne null and cause > 0}">
                            <c:if test="${cause == '1'}">
                                <div class="text-danger">Incorrect login or password!</div>
                            </c:if>
                            <c:if test="${cause == '2'}">
                                <div class="text-danger">You should login before start to work with application</div>
                            </c:if>
                            <c:if test="${cause == '3'}">
                                <div class="text-danger">Yoy have already login in other place</div>
                            </c:if>
                        </c:if>
                    </c:if>
                    <input name="submit" type="submit" class="btn btn-default" value="Ok"/>
                </form>
            </div>
            <div class="panel-footer">
                <a href="<c:url value="/join"/>">Need an account? Sign up free</a>
            </div>
        </div>
    </div>
</section>

<!-- add footer -->
<%@ include file="include/footer.jsp" %>
<!-- add footer -->

</body>
</html>
